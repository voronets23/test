import {Component, OnInit, ViewChild} from '@angular/core';
import {ChartModel} from '../chart-model';
import {ChartsService} from '../charts.service';
import {MatSort} from '@angular/material';
import {MatDialog} from '@angular/material';
import {ChartEditComponent} from '../chart-edit/chart-edit.component';
import { MatPaginator, MatTableDataSource} from '@angular/material';
import {SelectionModel} from '@angular/cdk/collections';

@Component({
    selector: 'app-chart-list',
    templateUrl: './chart-list.component.html',
    styleUrls: ['./chart-list.component.css']
})
export class ChartListComponent implements OnInit {
    displayedColumns: string[] = ['select', 'location', 'owner', 'type', 'date', 'as2', 'act', 'error', 'note', 'sampleNote'];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    chartModel: ChartModel[];
    selectedChart = [];
    dataSource: MatTableDataSource<ChartModel>;
    selection = new SelectionModel<ChartModel>(true, []);
    dataModel: ChartModel[] = [];
    constructor(
        private service: ChartsService,
        public dialog: MatDialog) {
    }

    ngOnInit() {
        this.service.getCharts().subscribe((charts) => {
            this.chartModel = charts;
            // this.sortedData = charts.slice();
            this.dataSource = new MatTableDataSource<ChartModel>(charts);
        });
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    addSelection(row) {
        if (this.dataModel.indexOf(row) !== -1) {
            this.dataModel.splice(this.dataModel.indexOf(row), 1);
        } else {
            this.dataModel.push(row);
        }
    }
    openDialog(e): void {
        const dialogRef = this.dialog.open(ChartEditComponent, {
            height: '500px',
            width: '50%',
            data: this.dataModel
        });

        dialogRef.afterClosed().subscribe(result => {
            if (!result) return;
            result.charts.forEach((item, i) => {
                this.chartModel.forEach((chart, chartIndex) => {
                    if (chart.id === item.id) {
                        this.chartModel[chartIndex] = item;
                    }
                });
            });
            this.dataSource.data = this.chartModel;
            this.selection.clear();
        });
    }

    get ifSelected() {
        let value = false;
        if (this.dataModel.length) {
            value = true;
        }
        return value;
    }
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
    masterToggle() {
        this.isAllSelected() ?
            this.selection.clear() :
            this.dataSource.data.forEach(row => this.selection.select(row));
    }
    applyFilter(filterValue: string) {
        if (!filterValue) {
            this.dataSource.filter = '';
        } else {
            this.dataSource.filter = filterValue.trim().toLowerCase();
        }
    }
}


