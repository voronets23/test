import {Component, OnInit, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {ChartModel} from '../chart-model';
import {FormBuilder, FormGroup, FormControl, FormArray} from '@angular/forms';

@Component({
    selector: 'app-chart-edit',
    templateUrl: './chart-edit.component.html',
    styleUrls: ['./chart-edit.component.css']
})
export class ChartEditComponent implements OnInit {
    charModel: ChartModel;
    chartForm = this.fb.group({
        charts: this.fb.array([
            this.fb.control('')
        ])
    });

    constructor(
        public dialogRef: MatDialogRef<ChartEditComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ChartModel,
        private fb: FormBuilder) {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    ngOnInit() {
        this.charModel = this.data;
        this.setAddresses(this.data);
    }

    setAddresses(charts) {
        const chartsFGs = charts.map(chart => this.fb.group(chart));
        const chartsFormArray = this.fb.array(chartsFGs);
        this.chartForm.setControl('charts', chartsFormArray);
    }
    get charts() {
        return this.chartForm.get('charts') as FormArray;
    }

}
