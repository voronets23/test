export class ChartModel {
    constructor(
        public id: number,
        public location?: string,
        public owner?: string,
        public type?: string,
        public date?: any,
        public as2?: string,
        public act?: number,
        public error?: string,
        public note?: string,
        public sampleNote?: string,
        ) {
    }
}
