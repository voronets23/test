import {Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-chart-result',
    templateUrl: './chart-result.component.html',
    styleUrls: ['./chart-result.component.css']
})
export class ChartResultComponent implements OnInit {
    public doughnutChartLabels: string[] = ['Download Sales', 'In-Store Sales', 'Mail-Order Sales'];
    public doughnutChartData: number[] = [350, 450, 100];
    public doughnutChartType = 'pie';

    constructor() {
    }
    ngOnInit() {
    }
}
