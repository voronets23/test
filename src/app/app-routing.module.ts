import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartResultComponent} from './chart-result/chart-result.component';
import { ChartListComponent} from './chart-list/chart-list.component';

const routes: Routes = [
    { path: 'result', component: ChartResultComponent },
    { path: 'list', component: ChartListComponent },
];

@NgModule({
    exports: [ RouterModule ],
    imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {
  constructor() {
  }
}
